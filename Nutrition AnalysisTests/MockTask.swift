//
//  MockTask.swift
//  Nutrition AnalysisTests
//
//  Created by Ahmed Ezz on 31/05/2021.
//

import Foundation

class MockTask: URLSessionDataTask {
    private let data: Data?
    private let urlResponse: URLResponse?
    private let responseError: Error?
    
    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)?
    
    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        self.data = data
        self.urlResponse = urlResponse
        self.responseError = error
    }
    
    override func resume() {
        DispatchQueue.main.async {[weak self] in
            guard let self = self else {return}
            self.completionHandler?(self.data, self.urlResponse, self.responseError)
        }
    }
}
