//
//  MockSession.swift
//  Nutrition AnalysisTests
//
//  Created by Ahmed Ezz on 31/05/2021.
//

import Foundation

class MockURLSession: URLSession {
    
    private let mockTask: MockTask
    
    init(data: Data?, urlResponse: URLResponse?, error: Error?) {
        mockTask = MockTask(data: data, urlResponse: urlResponse, error: error)
    }
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        mockTask.completionHandler = completionHandler
        return mockTask
    }
}
