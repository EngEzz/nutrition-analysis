//
//  Nutrition_AnalysisTests.swift
//  Nutrition AnalysisTests
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import XCTest
import RxSwift
@testable import Nutrition_Analysis

class Nutrition_AnalysisTests: XCTestCase {
    
    var disposeBag: DisposeBag?
    
    var ingredientRespository: IngredientRepositry?
    
    override func setUpWithError() throws {
        disposeBag = DisposeBag()
        ingredientRespository = IngredientRepositry()
    }

    override func tearDownWithError() throws {
        disposeBag = nil
        ingredientRespository = nil
    }
    
    func testIngredientDetailsSuccessReturn() {
      let bundle = Bundle(for: type(of: self))
      let path = bundle.path(forResource: "JsonTestData", ofType: "json")
      let jsonData = try? Data(contentsOf: URL(fileURLWithPath: path ?? ""))
      let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, error: nil)
      ingredientRespository?.session = mockURLSession
      let detailsExpectation = expectation(description: "IngredientDetails")
      let testValues = ["1 medium onion, peeled and chopped"]
      var ingredientDetailsResponse: Ingredient?
      let _ = ingredientRespository?.getIngredientDetails(values: testValues).bind{[weak self] ingredientDetails in
        guard let _ = self else {return}
            ingredientDetailsResponse = ingredientDetails
            detailsExpectation.fulfill()
      }.disposed(by: disposeBag!)
      waitForExpectations(timeout: 5) { (error) in
        XCTAssertNotNil(ingredientDetailsResponse)
      }
    }
    
    func testIngredientDetailsErrorReturn() {
      let error = NSError(domain: "error", code: 12, userInfo: nil)
      let mockURLSession  = MockURLSession(data: nil, urlResponse: nil, error: error)
      ingredientRespository?.session = mockURLSession
      let errorExpectation = expectation(description: "error")
      var errorResponse: Error?
      let _ = ingredientRespository?.getIngredientDetails(values: []).subscribe(onNext: nil, onError: {[weak self] error in
            guard let _ = self else {return}
            errorResponse = error
            errorExpectation.fulfill()
        }).disposed(by: disposeBag!)
      waitForExpectations(timeout: 5) { (error) in
        XCTAssertNotNil(errorResponse)
      }
    }
}
