//
//  NetworkManager.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//


import RxSwift

class NetworkManager: NSObject, NetworkManagerProtocol {
    
    //MARK: - Constants
    private let baseUrl = "https://api.edamam.com/api/"
    var session: URLSession!
    static let shared = NetworkManager()
    
    //MARK: - CreateRequest
    func createRequest<T:Codable>(decodedType: T.Type, urlPath: String, params: [String:Any], method: RequestMethod)->Observable<T> {
        return Observable.create({[weak self] observer in
            let queryParams = "app_id=\(Constants.appId.rawValue)&app_key=\(Constants.appKey.rawValue)"
            guard let url = URL(string: "\(self?.baseUrl ?? "" )\(urlPath)?\(queryParams)") else {return Disposables.create()}
            var urlRequest = URLRequest(url:  url)
            urlRequest.httpMethod = method.rawValue
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
            urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
            
            let _ = self?.session.dataTask(with: urlRequest) { [weak self] data, response, error in
                do {
                    guard let _ = self else {return}
                    guard let data = data else {
                        if let error = error {
                            observer.onError(error)
                        }
                        return
                    }
                    let jsonData = try JSONDecoder().decode(decodedType, from: data)
                    observer.onNext(jsonData)
                } catch let error {
                    observer.onError(error)
                }
            }.resume()
            return Disposables.create()
        })
    }
    
}

//MARK: - NetworkManagerProtocol
protocol NetworkManagerProtocol: AnyObject {
    func createRequest<T:Codable>(decodedType: T.Type,urlPath: String, params: [String:Any], method: RequestMethod)->Observable<T>
}
