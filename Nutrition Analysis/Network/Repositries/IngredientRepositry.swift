//
//  IngredientRepositry.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import RxSwift

class IngredientRepositry: NSObject, IngredientRepositryProtocol {
    
    var session: URLSession = URLSession.shared
    
    //MARK: - Enums
    private enum EndPoint: String {
        case IngredientDetails = "nutrition-details"
    }
    
    private enum Entity: String {
        case IngredientKey = "ingr"
    }
    
    //MARK: - GetIngredientDetails
    func getIngredientDetails(values: [String]) -> Observable<Ingredient> {
        let params = [Entity.IngredientKey.rawValue:values]
        return Observable.create({[weak self] observer in
            NetworkManager.shared.session = self?.session
            let _ = NetworkManager.shared.createRequest(decodedType: Ingredient.self,urlPath: EndPoint.IngredientDetails.rawValue, params: params, method: .POST).subscribe(onNext:{[weak self] ingredientDetails in
                guard let _ = self else {return}
                observer.onNext(ingredientDetails)
            }, onError: {[weak self] error in
                guard let _ = self else {return}
                observer.onError(error)
            })
            return Disposables.create()
        })
    }
}

//MARK: - IngredientRepositryProtocol
protocol IngredientRepositryProtocol: AnyObject {
    func getIngredientDetails(values: [String])-> Observable<Ingredient>
    var session: URLSession{set get}
}
