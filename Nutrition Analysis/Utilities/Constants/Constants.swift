//
//  Constants.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

// MARK: - Constants
enum Constants: String {
    case appKey = "1b9eff7b18b36bb18fc3fc6ab3a55d52"
    case appId = "16d0fdd5"
}

// MARK: - RequestMethod
enum RequestMethod: String {
    case GET
    case POST
}
