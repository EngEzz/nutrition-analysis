//
//  IngredientListViewController.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

import RxSwift

import RxCocoa

class IngredientListViewController: UIViewController, BaseViewControllerProtocol {

    // MARK: - IBOutlets
    @IBOutlet private weak var showTotalNutritionButton: UIButton!
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - Variables
    private let viewModel: IngredientListViewModel!
    private let disposeBag = DisposeBag()
    
    // MARK: - Initilaize
    required init(viewModel: BaseViewModel) {
        self.viewModel = viewModel as? IngredientListViewModel
        super.init(nibName: "\(IngredientListViewController.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - InitViews
    private func initViews() {
        configureTableView()
    }
    
    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        bindTableView()
        bindButtonView()
        viewModel.viewDidLoad()
    }

    // MARK: - ConfigureTableView
    private func configureTableView() {
        tableView.register(UINib(nibName: "\(IngredientItemTableViewCell.self)", bundle: nil), forCellReuseIdentifier: "\(IngredientItemTableViewCell.self)")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80
    }
    
    // MARK: - BindTableView
    private func bindTableView() {
        
        viewModel.dataSource.bind(to: tableView.rx.items(cellIdentifier: "\(IngredientItemTableViewCell.self)", cellType: IngredientItemTableViewCell.self)) {[weak self] (row,item,cell) in
            guard let _ = self else {return}
            cell.selectionStyle = .none
            cell.asBaseCell().configureCell(data: item)
        }.disposed(by: disposeBag)
        
        tableView.rx.modelSelected(IngredientItemCellViewModel.self).subscribe(onNext: {[weak self] item in
            if item.getErrorMessage() != nil {
                return
            }
            let viewController = IngredientDetailsRouter.createDetailsViewController(ingredient: item.getIngredientDetails(), type: .Daily)
            self?.present(viewController, animated: true)
        }).disposed(by: disposeBag)
    }
    
    // MARK: - BindButtonView
    private func bindButtonView() {
        let _ = viewModel.canShowTotalObserver.bind(to: showTotalNutritionButton.rx.isEnabled)
        let _ = viewModel.canShowTotalObserver.map{$0 ? 1 : 0.1 }.bind(to: showTotalNutritionButton.rx.alpha)
    }
    
    // MARK: - IBAction
    @IBAction private func showTotalNutrition(_ sender: Any) {
        guard let ingredient = viewModel.totalIngredientDetails else {return}
        let viewController = IngredientDetailsRouter.createDetailsViewController(ingredient: ingredient, type: .Total)
        self.present(viewController, animated: true)
    }
}
