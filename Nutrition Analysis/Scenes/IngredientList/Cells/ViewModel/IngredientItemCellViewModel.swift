//
//  IngredientItemCellViewModel.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

class IngredientItemCellViewModel: BaseCellViewModel {
    
    // MARK: - Variables
    private let ingredient: Ingredient
    
    // MARK: - Initialize
    init(ingredient: Ingredient,cellIdentifier: String) {
        self.ingredient = ingredient
        super.init(cellIdentifier: cellIdentifier)
    }
    
    // MARK: - Helpers
    func getName()-> String {
        return ingredient.ingredientEntered ?? ""
    }
    
    func getCalories() -> String {
        return ingredient.calories?.description ?? ""
    }
    
    func getWeight() -> String {
        return ingredient.totalWeight?.description ?? ""
    }
    
    func getIngredientDetails()-> Ingredient {
        return ingredient
    }
    
    func showErrorMessage()-> Bool {
        return ingredient.totalWeight == nil 
    }
    
    func getErrorMessage()-> String? {
        return showErrorMessage() ? "low_quality" : nil
    }
}
