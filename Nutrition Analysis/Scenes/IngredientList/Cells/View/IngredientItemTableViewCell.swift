//
//  IngredientItemTableViewCell.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

class IngredientItemTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet private weak var weightLabel: UILabel!
    @IBOutlet private weak var caloriesLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var errorLabel: UILabel!
    
    // MARK: - ConfigureCell
    override func configureCell(data: BaseCellViewModel) {
        let viewModel = data as? IngredientItemCellViewModel
        self.nameLabel.text = viewModel?.getName()
        self.weightLabel.text = viewModel?.getWeight()
        self.caloriesLabel.text = viewModel?.getCalories()
        self.errorLabel.text = viewModel?.getErrorMessage() ?? ""
        self.errorLabel.isHidden = viewModel?.showErrorMessage() == false
        self.errorLabel.backgroundColor = viewModel?.showErrorMessage() == true ? .white : .clear
    }
}
