//
//  IngredientListViewModel.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import RxSwift

class IngredientListViewModel: BaseViewModel {
    
    // MARK: - Variables
    private var cellViewModels:[IngredientItemCellViewModel] = []
    
    var canShowTotalObserver = PublishSubject<Bool>()
    
    let dataSource = PublishSubject<[IngredientItemCellViewModel]>()
    
    private let repositry: IngredientRepositry!
    
    var totalIngredientDetails: Ingredient?
    
    // MARK: - Initialize
    init(ingredients:[Ingredient],repositry: IngredientRepositry) {
        self.repositry = repositry
        super.init()
        self.initCellViewModels(ingredients:ingredients)
    }
    
    // MARK: - InitCellViewModels
    private func initCellViewModels(ingredients:[Ingredient]) {
        let _ = ingredients.map{self.cellViewModels.append(IngredientItemCellViewModel(ingredient: $0,cellIdentifier: "\(IngredientItemTableViewCell.self)"))}
    }
    
    // MARK: - ViewDidLoad
    func viewDidLoad() {
        dataSource.onNext(self.cellViewModels)
        canShowTotalObserver.onNext(false)
        self.getTotalIngredientDetails()
        self.cellViewModels.enumerated().forEach({(index,item) in
            let _ = self.getIngredientDetails(ingredientValue: [item.getName()]).bind{[weak self] ingredientDetails in
                let ingredient = ingredientDetails
                ingredient.ingredientEntered = item.getName()
                self?.cellViewModels[index] = IngredientItemCellViewModel(ingredient: ingredient, cellIdentifier: "\(IngredientItemTableViewCell.self)")
                self?.dataSource.onNext(self?.cellViewModels ?? [])
            }
        })
    }
    
    // MARK: - GetTotalIngredientDetails
    private func getTotalIngredientDetails() {
        let totalIngredient = cellViewModels.map{$0.getName()}
        let _ = self.getIngredientDetails(ingredientValue: totalIngredient).bind{[weak self] totalIngredientDetails in
            self?.totalIngredientDetails = totalIngredientDetails
            self?.totalIngredientDetails?.ingredientEntered = totalIngredient.joined(separator: "\n")
            self?.canShowTotalObserver.onNext(true)
        }
    }
    
    // MARK: - GetIngredientDetails
    private func getIngredientDetails(ingredientValue: [String])-> Observable<Ingredient> {
        Observable.create({[weak self] observer in
            let _ = self?.repositry.getIngredientDetails(values: ingredientValue).bind{[weak self] ingredientDetails in
                guard let _ = self else {return}
                observer.onNext(ingredientDetails)
            }
            return Disposables.create()
        })
    }
}
