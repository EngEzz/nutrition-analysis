//
//  ViewController.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

import RxCocoa

class IngredientInputViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var ingredientInputTextView: UITextView!
    
    @IBOutlet private weak var startAnalysisButton: UIButton!
    
    // MARK: - Variables
    var previousRectPosition = CGRect.zero
    
    private let viewModel = IngredientInputViewModel()
    
    // MARK: - InitViews
    private func initViews() {
        startAnalysisButton.isEnabled = false
        startAnalysisButton.alpha = 0.1
    }
    
    // MARK: - InitObservers
    private func initObservers() {
        let _ = ingredientInputTextView.rx.text.bind(onNext: {[weak self] value in
            self?.viewModel.addNewGredient(value: value ?? "")
        })
        
        let _ = viewModel.isGredientValid.bind(to: startAnalysisButton.rx.isEnabled)
        let _ = viewModel.isGredientValid.map{$0 ? 1: 0.1}.bind(to: startAnalysisButton.rx.alpha)
        
        let _ = viewModel.pushToView.bind(onNext: {[weak self] in
            let viewController = IngredientLstRouter.createListViewController(ingredients: self?.viewModel.ingredients ?? [])
            self?.present(viewController, animated: true, completion: nil)
        })
    }
    
    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initViews()
        self.initObservers()
    }
    
    // MARK: - IBAction
    @IBAction private func startAnalysis(_ sender: Any) {
        viewModel.removeAllIngredients()
        viewModel.startAnalysis()
    }
}

