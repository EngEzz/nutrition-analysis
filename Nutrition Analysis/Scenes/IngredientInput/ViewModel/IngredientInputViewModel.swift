//
//  IngredientInputViewModel.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import RxSwift

class IngredientInputViewModel: BaseViewModel {
    
    // MARK: - Variables
    var isGredientValid = PublishSubject<Bool>()
    var ingredients:[Ingredient] = []
    private var ingredientValue = ""
    
    // MARK: - addNewGredient
    public func addNewGredient(value: String) {
        self.ingredientValue = value
        let values = value.replacingOccurrences(of: "\"", with: "").split(separator: "\n")
        let isValueValidCount = values.filter{self.isValidGredient(value: $0.description) == true}.count
        let isValid = values.count == isValueValidCount
        self.isGredientValid.onNext(isValid)
    }
    
    // MARK: - StartAnalysis
    func startAnalysis() {
        let ingredients = ingredientValue.split(separator: "\n")
        let _ = ingredients.map{[weak self] value in
            self?.ingredients.append(Ingredient(ingredientEntered: value.description))
        }
        self.pushToView.onNext(())
    }
    
    // MARK: - IsValidGredient
    private func isValidGredient(value: String)-> Bool {
        let regex = "^[0-9]+\\s.*"
        let textTest = NSPredicate(format:"SELF MATCHES %@", regex)
        return textTest.evaluate(with: value)
    }
    // MARK: - RemoveAllIngredients
    func removeAllIngredients() {
        ingredients.removeAll()
    }
}
