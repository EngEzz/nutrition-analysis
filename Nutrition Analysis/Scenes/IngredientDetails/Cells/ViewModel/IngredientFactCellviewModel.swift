//
//  IngredientFactCellviewModel.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

class IngredientFactCellviewModel: BaseCellViewModel {
    
    // MARK: - Variables
    private let fact: Fact
    
    // MARK: - Initilaize
    init(fact: Fact,cellIdentifier: String) {
        self.fact = fact
        super.init(cellIdentifier: cellIdentifier)
    }
    
    // MARK: - Helpers
    func getFactName()-> String {
        return fact.label ?? ""
    }
    
    func getFactValue()-> String {
        return fact.quantity?.description ?? ""
    }
}
