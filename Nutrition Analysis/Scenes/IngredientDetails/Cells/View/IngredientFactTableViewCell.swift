//
//  IngredientFactTableViewCell.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

class IngredientFactTableViewCell: UITableViewCell {

    // MARK: - IBOutlets
    @IBOutlet private weak var factValueLabel: UILabel!
    @IBOutlet private weak var factNameLabel: UILabel!
    
    // MARK: - ConfigureCell
    override func configureCell(data: BaseCellViewModel) {
        let viewModel = data as? IngredientFactCellviewModel
        self.factValueLabel.text = viewModel?.getFactValue()
        self.factNameLabel.text = viewModel?.getFactName()
    }
}
