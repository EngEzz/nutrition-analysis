//
//  IngredientDetailsViewController.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit
import RxSwift
import RxCocoa

class IngredientDetailsViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var nameLabel: UILabel!
    
    @IBOutlet private weak var detailsTableView: UITableView!
    
    // MARK: - Variables
    private let viewModel: IngredientDetailsViewModel!
    private let disposeBag = DisposeBag()
    
    // MARK: - Initilaize
    required init(viewModel: BaseViewModel) {
        self.viewModel = viewModel as? IngredientDetailsViewModel
        super.init(nibName: "\(IngredientDetailsViewController.self)", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - InitViews
    private func initViews() {
        configureTableView()
    }
    
    // MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        bindTableView()
        bindData()
        viewModel.viewDidLoad()
    }
    
    // MARK: - ConfigureTableView
    private func configureTableView() {
        detailsTableView.register(UINib(nibName: "\(IngredientFactTableViewCell.self)", bundle: nil), forCellReuseIdentifier: "\(IngredientFactTableViewCell.self)")
        detailsTableView.rowHeight = UITableView.automaticDimension
        detailsTableView.estimatedRowHeight = 50
    }
    
    // MARK: - BindTableView
    private func bindTableView() {
        
        viewModel.dataSource.bind(to: detailsTableView.rx.items(cellIdentifier: "\(IngredientFactTableViewCell.self)", cellType: IngredientFactTableViewCell.self)) {[weak self] (row,item,cell) in
            guard let _ = self else {return}
            cell.selectionStyle = .none
            cell.asBaseCell().configureCell(data: item)
        }.disposed(by: disposeBag)
    }
    
    // MARK: - BindData
    private func bindData() {
        let _ = viewModel.nameObserver.bind(to:nameLabel.rx.text)
    }
}
