//
//  IngredientDetailsViewModel.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import RxSwift

class IngredientDetailsViewModel: BaseViewModel {
    
    // MARK: - Variables
    private let ingredientDetails: Ingredient
    var cellViewModels = [IngredientFactCellviewModel]()
    
    let dataSource = PublishSubject<[IngredientFactCellviewModel]>()
    
    let nameObserver = PublishSubject<String>()
    
    let requiredKeys = "Calories,Fat,Cholesterol,Sodium,Carbs,Protein,Vitamin,Calcium,Iron,Potassium"
    
    // MARK: - Initilize
    init(ingredient: Ingredient) {
        self.ingredientDetails = ingredient
        super.init()
        self.initCellViewModels(nutrients: ingredientDetails.totalNutrients)
    }
    
    // MARK: - InitCellViewModels
    func initCellViewModels(nutrients: [String:Fact]?) {
        self.cellViewModels.removeAll()
        let _ = ingredientDetails.totalNutrients?.map{[weak self] item in
            let newValue = item.value.label?.split(separator: " ").first?.description ?? ""
            if requiredKeys.lowercased().contains(newValue.lowercased()) {
                self?.cellViewModels.append(IngredientFactCellviewModel(fact: item.value, cellIdentifier: "\(IngredientFactTableViewCell.self)"))
            }
        }
        let caloriesFact = Fact(label: "Calories", quantity: ingredientDetails.calories , unit: nil)
        self.cellViewModels.insert(IngredientFactCellviewModel(fact: caloriesFact, cellIdentifier: "\(IngredientFactTableViewCell.self)"),at:0)
    }
    
    // MARK: - ViewDidLoad
    func viewDidLoad() {
        dataSource.onNext(cellViewModels)
        nameObserver.onNext(ingredientDetails.ingredientEntered ?? "")
    }
}
