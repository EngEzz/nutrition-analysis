//
//  IngredientDetailsDailyViewModel.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 30/05/2021.
//

import UIKit

class IngredientDetailsDailyViewModel: IngredientDetailsViewModel {
    
    // MARK: - Initiliaze
    override init(ingredient: Ingredient) {
        super.init(ingredient: ingredient)
        self.initCellViewModels(nutrients: ingredient.totalDaily)
    }

}
