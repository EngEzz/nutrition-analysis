//
//  BaseViewControllerProtocol.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

protocol BaseViewControllerProtocol: AnyObject {
    init(viewModel: BaseViewModel)
}
