//
//  BaseCellViewProtocol.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

protocol BaseCellViewProtocol: AnyObject {
    func configureCell(data: BaseCellViewModel)
}
 
extension UITableViewCell: BaseCellViewProtocol {
    @objc func configureCell(data: BaseCellViewModel) {
        
    }
    
    func asBaseCell()-> BaseCellViewProtocol {
        return self as BaseCellViewProtocol
    }
}
