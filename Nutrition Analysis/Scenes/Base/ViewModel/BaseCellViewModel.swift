//
//  BaseCellViewModel.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

class BaseCellViewModel: NSObject {
    
    var cellIdentifier: String
    
    init(cellIdentifier: String) {
        self.cellIdentifier = cellIdentifier
        super.init()
    }
}
