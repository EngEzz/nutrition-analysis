//
//  BaseViewModel.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import RxSwift

class BaseViewModel {
    
    var pushToView = PublishSubject<Void>()
}
