//
//  IngredientDetailsRouter.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 30/05/2021.
//

import UIKit

class IngredientDetailsRouter: IngredientDetailsRouterProtocol {
    
    //MARK: - CreateDetailsViewController
    static func createDetailsViewController(ingredient: Ingredient, type: IngredientDetailsType)-> UIViewController {
        let viewModel = type == .Daily ? IngredientDetailsDailyViewModel(ingredient: ingredient) : IngredientDetailsViewModel(ingredient: ingredient)
        let viewController = IngredientDetailsViewController(viewModel: viewModel)
        return viewController
    }
}

//MARK: - IngredientDetailsRouterProtocol
protocol IngredientDetailsRouterProtocol: AnyObject {
    static func createDetailsViewController(ingredient: Ingredient, type: IngredientDetailsType)-> UIViewController
}

//MARK: - IngredientDetailsTypeEnum
enum IngredientDetailsType: String {
    case Total
    case Daily
}
