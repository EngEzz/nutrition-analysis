//
//  IngredientLstRouter.swift
//  Nutrition Analysis
//
//  Created by Ahmed Ezz on 29/05/2021.
//

import UIKit

class IngredientLstRouter: IngredientListRouterProtocol {
    
    //MARK: - CreateListViewController
    static func createListViewController(ingredients: [Ingredient])-> UIViewController {
        let repositry = IngredientRepositry()
        let viewModel = IngredientListViewModel(ingredients: ingredients,repositry: repositry)
        let viewController = IngredientListViewController(viewModel: viewModel) 
        return viewController
    }
}

//MARK: - IngredientListRouterProtocol
protocol IngredientListRouterProtocol: AnyObject {
    static func createListViewController(ingredients: [Ingredient])-> UIViewController
}
